""" Main module
"""

import fire
import logging
from scraper import PeopleSearchExpertScraper


logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s %(name)s: %(message)s", datefmt='%y/%m/%d %H:%M:%S')


def main(first_name, last_name, middle_initial=None, city=None, state=None):
    scraper = PeopleSearchExpertScraper()
    urls = scraper.find_urls(first_name, last_name, middle_initial=middle_initial, city=city, state=state)
    
    if not urls:
        print("No urls found")
        return

    print("%s url(s) found:" % len(urls))
    for url in urls:
        print(url)

if __name__ == '__main__':
    fire.Fire(main)
