""" Scraper module
"""

import requests
import logging
from bs4 import BeautifulSoup


log = logging.getLogger("Scraper")


class PeopleSearchExpertScraper():
    """ This class has the responsibility to find possible urls 
    in https://www.peoplesearchexpert.com that match the given pii.
    """

    def find_urls(self, first_name, last_name, middle_initial=None, city=None, state=None):
        """ Search urls containing the given pii.
        """

        url = "https://www.peoplesearchexpert.com/"
        page = 1 # The results are paginated, starting at first page.
        urls = set() # Set to store urls that match to the given pii.

        # Urls parameters
        params = {
            "q[full_name]": "%s %s" % (first_name, last_name),
            "q[location]": "",
        }

        # Add location information
        if city or state:
            if city and state:
                params.update({
                    "q[location]": "%s, %s" % (city, state)
                })
            
            else:
                log.warning("You must set City and State. Ignoring location on search.")

        while True:
            # Set the current page
            params["page"] = page

            response = requests.get(url, params=params)
            content = response.content.decode()
            
            soup = BeautifulSoup(content, 'html.parser')

            people_results = soup.find("div", {"id": "search-results"}).select(".element")

            if not people_results:
                # There's no more results to look at.
                break
            
            # Iterate the results.
            for people in people_results:
                fullname = people.select("h3")[0].get_text()

                names = fullname.split(" ")
                match = names[0].lower().strip() == first_name.lower()
                match &= names[-1].lower().strip() == last_name.lower()

                if middle_initial:
                    match &= len(names) > 2 and names[1].lower().startswith(middle_initial.lower())

                if match:
                    log.debug(fullname)
                    urls.add(people.select("a")[0].get("href"))
            
            page += 1
        return urls
