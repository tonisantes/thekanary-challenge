# thekanary-challenge

Task: Write a simple program for querying the given website for the given pii. If the given pii is present on the website, return the url where the information is listed. If the given pii is not present on the website, indicate that in the program's output. Use the languages and packages you are most confident with.

Site to query: https://www.peoplesearchexpert.com/people/

### 1. first pii set:
- First Name: Bob
- Last Name: Smith
- Middle Initial: [Unknown]
- State: Texas
- City: Houston

### Solution
```bash
python run.py --first_name=Bob --last_name=Smith --city=Houston --state=TX
```

### 2.second pii set:
- First Name: Rob
- Last Name: Corbova
- Middle Initial: L
- State: Columbus
- City: Ohio

### Solution
```bash
python run.py --first_name=Rob --last_name=Corbova --middle_initial=L --city=Columbus --state=OH
```